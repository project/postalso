RELEASE
=======
Post Also module rewrote for Drupal 6.x
by Yi Yang <yang_yi_cn@yahoo.com>
2009/01/12 - Vancouver

Post Also module for Drupal 5.x
by Robin Millette <robin@millette.info>
2008/02/11 - Montreal
Free Software licenced under GPLv3 and GPLv2
More info: http://drupal.org/project/postalso

INITIAL ANNOUNCEMENT
====================
Post Also is a module for Drupal that lets you post simultaneously to multiple sites. For example, I often post content on my blog but since I'm too lazy to just copy/paste it to other sites, generally for an association where it would belong, the content lives solely on my site. Or it might eventually get sniffed up through RSS, but sometimes, that's not fast or good enough.

Post Also uses xmlrpc to talk to various web engines using so-called blog APIs. Currently, only metaWeblog is supported (allowing to post to other Drupal or Wordpress sites for example) but other APIs should soon follow (blogger, moveble type, atom) as well as autodiscovery. That means you will soon be able simultaneously post to other Drupal, Wordpress, TikiWiki, Plone, Xoops, Typo3 and many more supported web engines like CMSes, calendars and blogs.

Configuration happens in two stages. First, the site admin (given the "post also destinations" access right) can setup destinations with names, xmlrpc endpoints and supported content types. Next, each user (given the "post also remote accounts" access right) can setup his remote accounts giving a destination, username and password. After this is done, the node submit forms, if supported, will allow the user to post also using his other remote accounts. The admin can also edit the users remote accounts.

There was a module called blogclient for Drupal 4.6 originally written by chx for nowpublic and released by walkah, unfortunately I found it too late since it doesn't appear in the module directory anymore. I never used it but looking at the code, I note it did the configuration a little differently.

TODO
====
* let users edit their remote accounts
* let admin edit/delete destinations and appropriate types
* xmlrpc endpoint autodiscovery
* support more blog apis (blogger, moveble type, atom)
* map from one type to another
* support for drupal cck
* support for project module
* support for events/calendar
* edit/delete remote posts
* more phpdoc
* queue jobs
* create a few blocks (stats, available destinations, etc)
* port to Drupal 6
* make it expandable through submodules (to post specifically to wordpress, twitter, del.icio.us, etc. according to users needs)

