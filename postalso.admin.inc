<?php

/* helper function for the admin settings */
function postalso_admin_form() {
  $destinations = variable_get('postalso_destinations', array());
//  print_r($destinations); die;
  $known_destinations = '';
  foreach ($destinations as $destination) {
    $types = implode($destination['types'], ', ');
    $known_destinations .= "<li>{$destination['name']} ($types)</li>";
  }
  $form['postalso_know'] = array(
      '#type' => 'fieldset',
      '#title' => t('Known destinations'),
      '#prefix' => '<ol>',
      '#value' => $known_destinations,
      '#suffix' => '</ol>',
      '#collapsible' => true,
      );

  $form['postalso_where'] = array(
      '#type' => 'fieldset',
      '#title' => t('New destination'),
      '#collapsible' => true,
      '#collapsed' => true,
      );

  $form['postalso_where']['postalso_where_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Destination name'),
      '#default_value' => '',
      '#required' => true,
  );

  $form['postalso_where']['postalso_where_types'] = array(
      '#type' => 'select',
      '#title' => t('Select supported types'),
      '#default_value' => array(),
      '#options' => node_get_types('names'),
      '#multiple' => true,
      '#required' => true,
  );
  
//  $form['postalso_where']['postalso_where_type_remote'] = array(
//      '#type' => 'radios',
//  );

  $form['postalso_where']['postalso_where_url'] = array(
      '#type' => 'textfield',
      '#title' => t('URL of XMLRPC endpoint'),
      '#default_value' => '',
      '#description' => t('If http://example.com/ is your website, http://example.com/xmlrpc.php is often the correct URL.'),
      '#required' => true,
  );
  $form['postalso_where']['postalso_where_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Remote user for login'),
      '#default_value' => '',
      '#required' => true,
  );
  $form['postalso_where']['postalso_where_pass'] = array(
      '#type' => 'password',
      '#title' => t('Remote password for login.'),
      '#default_value' => '',
      '#required' => true,
  );

  $form['#validate'][] = 'postalso_admin_settings_form_validate';
  $form['#submit'] = array('postalso_admin_settings_form_submit');

  return system_settings_form($form);

}

function postalso_admin_settings_form_validate($form, &$form_values) {
  if (!valid_url($form_values['values']['postalso_where_url'], true)) {
    form_set_error('postalso_where_url', t('Invalid URL given.'));
  }
}

function postalso_admin_settings_form_submit($form, $form_values) {
  $destinations = variable_get('postalso_destinations', array());
  $name = $form_values['values']['postalso_where_name'];
  $destinations[$name] = array(
      'name' => $name,
      'types' => $form_values['values']['postalso_where_types'],
      'url' => $form_values['values']['postalso_where_url'],
      'user' => $form_values['values']['postalso_where_user'],
      'pass' => $form_values['values']['postalso_where_pass'],
      );
  variable_set('postalso_destinations', $destinations);
}


