<?php

function postalso_node_post($node_src) {
  $dst_name = '';
  $destinations = variable_get('postalso_destinations', array());
  //print_r($destinations);
  if (sizeof($destinations) == 0) {
    drupal_set_message(t("Destination site not set 1")); 
    return FALSE;
  }
  else if (sizeof($destinations) > 1) {
    if (!$dst_name) {
      drupal_set_message(t("Destination site not set 2")); 
      return FALSE;
    }
    else {
      //match the dst by name
      $dst = $destinations[$dst_name];
    }
  }
  else {
    if (!$dst_name) {
      $dst = array_pop($destinations);
    }
    else {
      //match the dst by name
      $dst = $destinations[$dst_name];
    }
  }
//  print_r($destinations);
//  print_r($dst);

  if (!$dst) {
    drupal_set_message(t("Destination site not found")); 
    return FALSE;
  }

  $dst['uid'] = 1; //TODO
  $node = _postalso_format_xmlrpc_node_save($node_src, $dst);

  // assume the remote site is a Drupal site, other types TODO
  $result = xmlrpc($dst['url'], "user.login", ' ', $dst['user'], $dst['pass']);
  if ($result) {
  //print_nice($result);
    if (is_array($result)) {
      $sid = $result['sessid'];
    }
    //print $sid;
  }
  else {
    print 'user error';
    print $dst['url'] ."<br>\n";
    print "user.login" ."<br>\n";
    print $dst['user'] ."<br>\n";
    print $dst['pass'] ."<br>\n";
    print xmlrpc_errno() ."<br>\n";
    print xmlrpc_error_msg() ."<br>\n";
    return FALSE;
  }
  $result = xmlrpc($dst['url'], "user.login", ' ', $dst['user'], $dst['pass']);
  if ($result) {
  //print_nice($result);
    if (is_array($result)) {
      $sid = $result['sessid'];
    }
    //print $sid;
  }
  else {
    print 'user error';
    print $dst['url'] ."<br>\n";
    print "user.login" ."<br>\n";
    print $dst['user'] ."<br>\n";
    print $dst['pass'] ."<br>\n";
    print xmlrpc_errno() ."<br>\n";
    print xmlrpc_error_msg() ."<br>\n";
    return FALSE;
  }


  $result = xmlrpc($dst['url'], "node.save", $sid, $node);
  if ($result) {
    $nid_dst = $result;
    // save to the postalso table, TODO
  }
  else {
    print 'node save error';
    print xmlrpc_errno();
    print xmlrpc_error_msg();
  }


}

function _postalso_format_xmlrpc_node_save($node_src, $dst = array()) {
//  print("<pre>$nid");print_r($node_src);  die;
  $node_type_dst = 'page'; // should make it setable, TODO
  $node_dst = array(
                "type" => $node_type_dst,     //required
                "uid" => $dst['uid'],   //required both uid and name to associate to a user
                "name" => $dst['user'], //required both uid and name to associate to a user

                "title" => $node_src->title,
                "body" => $node_src->body,
        );
  return $node_dst;
}
